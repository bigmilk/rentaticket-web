'use strict';

/* App Module */

// создаём модуль приложения
var app = angular.module('onedaypass', ['onedaypassServices', 'ui']).
    config(['$routeProvider', function($routeProvider) {
    $routeProvider.
        when('/', {templateUrl: 'partials/intro.html',   controller: IntroCtrl}).
        when('/search', {templateUrl: 'partials/search.html', controller: SearchTicketsCtrl}).
        when('/profile', {templateUrl: 'partials/profile.html', controller: ProfileCtrl}).
        otherwise({redirectTo: '/'});
}]).
    config(['$locationProvider', function($locationProvider) {
    $locationProvider.html5Mode(true).hashPrefix('!');
}]);

// делаем календарь русским
$.datepicker.setDefaults( $.datepicker.regional[ 'ru' ] );