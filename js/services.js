'use strict';

/* Services */

angular.module('onedaypassServices', ['ngResource'])
    .factory('Ticket', function($resource, $window) {
        return $resource($window.appConfig.api + 'tickets/:userId/:placeId/:date', {}, {});
    })

    .factory('Place', function($resource, $window) {
        return $resource($window.appConfig.api + 'places/:slug', {}, {});
    })

    .factory('User', function($resource, $window) {
        var resource = $resource($window.appConfig.api + 'users/:id', {}, {});
        var currentUser = {loaded: false};

        // добавляем метод для загрузки текущего юзера
        resource.getCurrentUser = function ($scope, user, force, callback) {
            force = force || false;
            $scope[user] = currentUser;

            if (!currentUser.loaded || force) {
                $scope[user] = resource.get({id: 'current'}, function () {
                    $scope[user].loaded = true;
                    currentUser = $scope[user];

                    if (callback && callback instanceof Function) {
                        callback(currentUser);
                    }
                });
            }
        }

        return resource;
    })

    .factory('DateUtils', function () {
        return {
            /**
             * Формирует даты на текущую неделю
             * @return {Array}
             */
            getDatesForWeek: function () {
                var dateNow = new Date();
                var dates = [dateNow.getDate() + "/" + (dateNow.getMonth()+1) + "/" + dateNow.getFullYear()];
                for (var day=0; day < 6; day++) {
                    dateNow.setDate(dateNow.getDate() + 1);
                    dates.push(dateNow.getDate() + "/" + (dateNow.getMonth()+1) + "/" + dateNow.getFullYear());
                }
                return dates;
            },

            /**
             * Возвращает стандартный формат даты для проекта
             * @return {RegExp}
             */
            getDatePattern: function () {
                return /(\d+)\/(\d+)\/(\d+)/;
            },

            /**
             * Возвращает русские имена для дней недели
             * @return {Array}
             */
            getWeeksRussianNames: function () {
                return ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'];
            },

            /**
             * Возвращает русские имена для месяцев
             * @return {Array}
             */
            getMonthsRussianNames: function () {
                return ['янв', 'фев', 'мар', 'апр', 'май', 'июн', 'июл', 'авг', 'сен', 'окт', 'ноя', 'дек'];
            }
        }
    })
;