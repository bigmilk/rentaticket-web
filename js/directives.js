'use strict';

/* App Directives */

app.directive('datepicker', function($parse) {
    return function(scope, element, attrs) {
        var ngModel = $parse(attrs.ngModel);
        $(function() {
            element.datepicker({
                inline: true,
                defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 1,
                dateFormat: "d/m/yy",
                onSelect: function(dateText) {
                    scope.$apply(function(scope){
                        ngModel.assign(scope, dateText);
                    });
                }
            });
        });
    }
});