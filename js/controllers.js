'use strict';

/* Controllers */

// контроллер главной
function IntroCtrl($scope, $http, DateUtils)
{
    // мутим фон
    $.vegas({
        src: window.appConfig.cdn + 'media/images/intro_pics/1.jpg'
    });
    $.vegas('overlay', {
        src: window.appConfig.cdn + 'lib/vegas/overlays/13.png'
    });

    // запрашиваем билеты на неделю
    $http.get(window.appConfig.api + 'tickets/week').success(function(data) {
        if (data.success) {
            // даты на текущую неделю
            var weekDates = DateUtils.getDatesForWeek();

            // распихиваем даты по местам
            var datePattern = DateUtils.getDatePattern();
            var places = [];
            for (var placeInx=0; placeInx < data.items.length; placeInx++) {
                if (!(placeInx in data.items)) continue;

                var item = data.items[placeInx];

                // объект места
                var place = {
                    'slug':   item.slug,
                    'name':   item.name,
                    'color':  item.color,
                    'days':   []
                };

                // формируем даты
                for (var j=0; j < weekDates.length; j++) {
                    var dateMatch = datePattern.exec(weekDates[j]);
                    var date = new Date(dateMatch[3], dateMatch[2]-1, dateMatch[1]);

                    var dateLong = weekDates[j];
                    var dateShort = date.getDate() + '.' + (date.getMonth()+1);

                    place.days.push ({
                        "date":     dateShort,
                        "full":     dateMatch[1] + '/' + dateMatch[2] + '/' + dateMatch[3],
                        "count":    item.dates.hasOwnProperty(dateLong) ? item.dates [dateLong] : 0,
                        "day":      DateUtils.getWeeksRussianNames() [date.getDay()],
                        "holiday":  (date.getDay() + 6) % 7 > 4
                    });
                }
                places.push(place);
            }

            $scope.places = places;
        }
    });
}

// контроллер поиска предложений
function SearchTicketsCtrl($scope, $routeParams, Ticket, DateUtils)
{
    // создаём карту
    var map = mapbox.map('map');
    map.addLayer(mapbox.layer().id('tmcw.map-2f4ad161'));
    map.zoom(13).center({ lat: 59.939134, lon: 30.312023 });
    map.ui.zoomer.add();
    map.ui.zoombox.add();
    map.ui.attribution.add()
        .content('<a href="http://mapbox.com/about/maps">Terms &amp; Feedback</a>');

    // мутим слой с метками мест, где можно забрать билеты
    var ticketsLayer = mapbox.markers.layer();
    map.addLayer(ticketsLayer);
    mapbox.markers.interaction(ticketsLayer);

    // переопределяем фабрику метки с целью добавления интерактивности
    ticketsLayer.factory(function(m) {
        // получаем стандартный HTML-элемент для
        var elem = mapbox.markers.simplestyle_factory(m);

        // центрирование на метке при клике
        MM.addEvent(elem, 'click', function() {
            map.ease.location({
                lat: m.geometry.coordinates[1],
                lon: m.geometry.coordinates[0]
            }).zoom(map.zoom()).optimal();
        });

        // выделяем в списке предложения, соответствующие метке
        MM.addEvent(elem, 'mouseover', function() {
            for (var i in m.properties.indexes) {
                $scope.tickets [m.properties.indexes[i].ticket].selected = true;
            }
            $scope.$apply();
        });

        // снимаем выделение с предложений в списке
        MM.addEvent(elem, 'mouseout', function(e) {
            for (var i in m.properties.indexes) {
                $scope.tickets [m.properties.indexes[i].ticket].selected = false;
            }
            $scope.$apply();
        });

        return elem;
    });

    // даты текущего дня и дня конца недели от текущего
    var today = new Date();
    var week  = new Date();
    week.setDate(today.getDate() + 6);

    // список билетов
    $scope.tickets = [];

    // модель пагинатора
    $scope.paginator = {};
    $scope.paginator.currentPage = 0;
    $scope.paginator.pageSize = 3;
    $scope.paginator.numberOfPages = function() {
        return $scope.tickets.length ? Math.ceil($scope.tickets.length / $scope.paginator.pageSize) : 0;
    };

    // модель поиска
    $scope.search = {};
    $scope.search.from = $routeParams.from || (today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear());
    $scope.search.to = $routeParams.to || (week.getDate() + '/' + (week.getMonth()+1) + '/' + week.getFullYear());
    $scope.search.usePeriod = $scope.search.from !== $scope.search.to ? "true" : "false";
    $scope.search.place = $routeParams.place || '';

    // наблюдатели за изменением данных модели поиска
    $scope.$watch('search.from', searchTickets);
    $scope.$watch('search.to', searchTickets);
    $scope.$watch('search.place', searchTickets);
    $scope.$watch('search.usePeriod', searchTickets);

    // функция подсветки маркера
    $scope.ticketMarkerHighlight = function (ticketIndex, disable)
    {
        // флаг выключения подсветки
        disable = disable || false;
        updateTicketMarkers($scope.tickets, disable ? [] : [ticketIndex]);
    };

    // функция обновления списка предложений на основе критериев поиска
    function searchTickets ()
    {
        // устанавливаем конечную дату поиска в зависимости от флага usePeriod
        var to = $scope.search.usePeriod === "true" ? $scope.search.to : $scope.search.from;

        // запрашиваем билеты для новых критериев посика
        $scope.tickets = Ticket.query(
            {from: $scope.search.from, to: to, place: $scope.search.place}, function (tickets) {
                $scope.paginator.currentPage = 0;
                updateTicketMarkers(tickets, [], true);
            }
        );

        // формируем человеко-понятную надпись о выбраном интервале дат
        updateDatesText();
    }

    // функция обновления состояния меток на карте
    function updateTicketMarkers(tickets, selected, updateExtent)
    {
        selected = selected || [];
        updateExtent = updateExtent || false;

        // удаляем все маркеры мест
        ticketsLayer.features([]);

        if (!tickets.length) return;

        var markers = [];
        var ticket;

        // формируем список уникальных месток на карте
        for (var i=0; i < tickets.length; i++) {
            // если у владельца предложения нет мест - ничего не добавляем
            ticket = tickets[i];
            if (!ticket.owner.places.length) {
                continue;
            }

            // проходимся по всем местам владельца
            for (var j=0; j < ticket.owner.places.length; j++) {
                // генерим ключ на основе координат места
                var key = ticket.owner.places[j].la + '_' + ticket.owner.places[j].lo;

                // если такого места нет - создаём
                if (!markers [key]) {
                    markers [key] = [];
                }

                // мобавляем описание места и билета
                markers [key].push({
                    ticket: i,
                    place: j
                });
            }
        }

        // добавляем полученный массив мест на карту
        for (i in markers) {
            // очередное место
            var item = markers [i];

            // данные о месте
            var place = tickets[item[0].ticket].owner.places[item[0].place];

            // информация о первом предложение по этому месту
            ticket = tickets[item[0].ticket];

            // проверяем, не выбран ли одно из предложений, к которому относится метка
            var markerSelected = false;
            if (selected.length) {
                for (j in item) {
                    var ticketIndex = item [j].ticket;

                    if (selected.indexOf(ticketIndex) !== -1) {
                        markerSelected = true;
                        break;
                    }
                }
            }

            // добавляем метку
            // todo: подумать о том, как выводить инфу в тултипе и нужен ли он вообще
            ticketsLayer.add_feature({
                geometry: {
                    'coordinates': [place.la, place.lo]
                },
                properties: {
                    'marker-color': markerSelected ? '#4AD453' : '#777',
                    'marker-symbol': 'star-stroked',
                    'title': ticket.owner.name,
                    'description': place.comment,
                    'indexes': item
                }
            });
        }

        // подгоняем масштаб карты так, чтобы были видны все метки
        if (updateExtent) {
            map.setExtent(ticketsLayer.extent());
        }
    }

    // функция формирует человеко-понятную надпись о выбраном интервале дат
    function updateDatesText() {
        var dateMatch, dateFrom, dateTo;

        // получаем объект минимальной даты
        dateMatch = DateUtils.getDatePattern().exec($scope.search.from);
        dateFrom = new Date(dateMatch[3], dateMatch[2] - 1, dateMatch[1]);

        // получаем объект максимально даты
        dateMatch = DateUtils.getDatePattern().exec($scope.search.to);
        dateTo = new Date(dateMatch[3], dateMatch[2] - 1, dateMatch[1]);

        // если ищём только на один день, забиваем на максимальный интервал
        if ($scope.search.usePeriod === "false" ||
            dateFrom.getTime() == dateTo.getTime() ||
            dateFrom.getTime() > dateTo.getTime()
        ) {
            $scope.search_dates = dateFrom.getDate() + ' ' + DateUtils.getMonthsRussianNames() [dateFrom.getMonth()];
            return;
        }

        // если месяцы одинаковы, выводим месяц один раз
        if (dateFrom.getMonth() == dateTo.getMonth()) {
            $scope.search_dates = dateFrom.getDate() + ' - ' + dateTo.getDate()
                + ' ' + DateUtils.getMonthsRussianNames() [dateFrom.getMonth()];
            return;
        }

        // если месяцы разные, выводим оба
        $scope.search_dates =
            dateFrom.getDate() + ' ' + DateUtils.getMonthsRussianNames() [dateFrom.getMonth()] + ' - ' +
            dateTo.getDate() + ' ' + DateUtils.getMonthsRussianNames() [dateTo.getMonth()];
    }

    // прочие переменные
}

// контроллер панели пользователя
function UserPanelCtrl($scope, User, $http)
{
    $scope.updateCurrentUser = function () {
        User.getCurrentUser($scope, 'user', true);
    };

    $scope.logout = function () {
        $http.get(window.appConfig.api + 'logout')
            .success(function () {$scope.updateCurrentUser();})
            .error(function () {$scope.updateCurrentUser();});
    };

    $scope.updateCurrentUser();
}

// контроллер авторизации
function LoginCtrl($scope, $http)
{
    // метод авторизации
    $scope.login = function () {
        $http({
            method: 'POST',
            url: window.appConfig.api + 'login/check',
            data: $.param({
                email: $scope.data.email,
                password: $scope.data.password
            }),
            headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'}
        })
            .success(function (data) {
                if (data.success) {
                    $scope.errors = [];
                    $scope.$parent.loginModal = false;

                    // @todo: move it to service
                    $scope.$$prevSibling.updateCurrentUser();
                } else {
                    $scope.errors = {global: data.message};
                }
            }
        );
    };
}

// контроллер регистрации
function RegisterCtrl($scope, $http)
{
    // метод регистрации
    $scope.register = function () {
        // сначала выполняем валидацию данных
        $http.post(window.appConfig.api + 'register', $scope.data)
            .success(function (data) {
                if (data.success) {
                    $scope.errors = [];
                    $scope.$parent.registerModal = false;
                    alert (data.message);
                } else {
                    $scope.errors = data.errors;
                }
            }
        );
    };
}

// контроллер профиля юзера
function ProfileCtrl($scope, User, Ticket, DateUtils)
{
    $scope.getTickets = function () {
        $scope.tickets = [];

        var tickets = Ticket.query({
            user: $scope.user.id
        });

        // даты на текущую неделю
        var dates = DateUtils.getDatesForWeek();

        // распихиваем даты по билетам
        var datePattern = DateUtils.getDatePattern();
        for (var ticketInx=0; ticketInx < tickets.length; ticketInx++) {
            var ticket = tickets[ticketInx];

            // формируем даты
            for (var j in dates) {
                var dateMatch = datePattern.exec(dates[j]);
                var date = new Date(dateMatch[3], dateMatch[2]-1, dateMatch[1]);

                var dateLong = dates[j];
                var dateShort = date.getDate() + '.' + (date.getMonth()+1);

                place.days.push ({
                    "date":     dateShort,
                    "full":     dateMatch[1] + '/' + dateMatch[2] + '/' + dateMatch[3],
                    "count":    ticket.dates.hasOwnProperty(dateLong) ? ticket.dates [dateLong] : 0,
                    "day":      window.weeks [date.getDay()],
                    "holiday":  (date.getDay() + 6) % 7 > 4
                });
            }
            tickets.push(place);
        }

        $scope.places = tickets;
    }

    User.getCurrentUser($scope, 'user', false, $scope.getTickets);
}